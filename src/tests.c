#include "mem_debug.h"
#include "mem.h"

#define INITIAL_HEAP_SIZE 10000

static struct block_header* heap;

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | additional_flags , 0, 0 );
}

void *test_heap_init() {
    heap = heap_init(INITIAL_HEAP_SIZE);
    if (heap == NULL) {
        debug("failed");
    }
    return heap;
}


void test1() {
    debug("Test 1\n");
    void* test1_allocated = _malloc(sizeof (int32_t) * 4);
    if (test1_allocated == NULL) debug("Memory allocation error\n");
    debug("Allocated pointer: %p\n", test1_allocated);
    int32_t* array = test1_allocated;
    debug_heap(stderr, heap);
    array[0] = 34;
    array[1] = 334;
    array[2] = 4566;
    debug("Array: %d, %d, %d, %d\n", array[0], array[1], array[2], array[3]);
    _free(test1_allocated);
    debug("Memory freed\n");
    debug_heap(stderr, heap);
    debug("Test 1 finished\n\n");
}

void test2() {
    debug("Test 2\n");
    void* test2_allocated1 = _malloc(1000);
    void* test2_allocated2 = _malloc(500);
    void* test2_allocated3 = _malloc(700);
    debug("Before free\n");
    debug_heap(stderr, heap);
    _free(test2_allocated3);
    debug("One block freed\n");
    debug_heap(stderr, heap);
    _free(test2_allocated2);
    _free(test2_allocated1);
    debug("Test 2 finished\n\n");
    
}

void test3() {
    debug("Test 3\n");
    void* test3_allocated1 = _malloc(300);
    void* test3_allocated2 = _malloc(200);
    void* test3_allocated3 = _malloc(700);
    void* test3_allocated4 = _malloc(600);
    debug("Before free\n");
    debug_heap(stderr, heap);
    debug("First and second blocks freed\n");
    _free(test3_allocated2);
    _free(test3_allocated1);
    debug_heap(stderr, heap);
    _free(test3_allocated4);
    _free(test3_allocated3);
    debug("Test 3 finished\n\n");	
}

void test4() {
debug("Test 4\n");
    void *test4_allocated1 = _malloc(10000);
    debug("First block allocated (large block)\n");
    debug_heap(stderr, heap);
    debug("Second block allocated\n");
    void *test4_allocated2 = _malloc(300);
    debug_heap(stderr, heap);
    _free(test4_allocated2);
    _free(test4_allocated1);
    debug("Memory freed\n");
    debug_heap(stderr, heap);
    debug("Test 4 finished\n\n");
}

void test5() {
    debug("Test 5\n");
    char* test5_allocated1 = _malloc(18000);
    debug_heap(stderr, heap);
    map_pages(test5_allocated1 + 15000, 4096, 0);
    char* test5_allocated2 = _malloc(5000);
    debug_heap(stderr, heap);
    _free(test5_allocated2);
    _free(test5_allocated1);
    debug_heap(stderr, heap);
    debug("Test 5 finished\n\n");
}

